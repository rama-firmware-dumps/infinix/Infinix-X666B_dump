#!/bin/bash

cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system_ext/app/AiGallery/AiGallery.apk.* 2>/dev/null >> system_ext/app/AiGallery/AiGallery.apk
rm -f system_ext/app/AiGallery/AiGallery.apk.* 2>/dev/null
cat system_ext/app/FaceID/FaceID.apk.* 2>/dev/null >> system_ext/app/FaceID/FaceID.apk
rm -f system_ext/app/FaceID/FaceID.apk.* 2>/dev/null
cat system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null >> system_ext/app/TranssionCamera/TranssionCamera.apk
rm -f system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null
cat system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk.* 2>/dev/null >> system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk
rm -f system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk.* 2>/dev/null
cat product/app/Gmail2/Gmail2.apk.* 2>/dev/null >> product/app/Gmail2/Gmail2.apk
rm -f product/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> product/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat product/app/Maps/Maps.apk.* 2>/dev/null >> product/app/Maps/Maps.apk
rm -f product/app/Maps/Maps.apk.* 2>/dev/null
cat product/app/Photos/Photos.apk.* 2>/dev/null >> product/app/Photos/Photos.apk
rm -f product/app/Photos/Photos.apk.* 2>/dev/null
cat product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null >> product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk
rm -f product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/YouTube/YouTube.apk.* 2>/dev/null >> product/app/YouTube/YouTube.apk
rm -f product/app/YouTube/YouTube.apk.* 2>/dev/null
cat product/operator/app/Duo/Duo.apk.* 2>/dev/null >> product/operator/app/Duo/Duo.apk
rm -f product/operator/app/Duo/Duo.apk.* 2>/dev/null
cat product/operator/app/FacebookStub/FacebookStub.apk.* 2>/dev/null >> product/operator/app/FacebookStub/FacebookStub.apk
rm -f product/operator/app/FacebookStub/FacebookStub.apk.* 2>/dev/null
cat product/operator/app/MusicMaker/MusicMaker.apk.* 2>/dev/null >> product/operator/app/MusicMaker/MusicMaker.apk
rm -f product/operator/app/MusicMaker/MusicMaker.apk.* 2>/dev/null
cat product/operator/app/PalmPay/PalmPay.apk.* 2>/dev/null >> product/operator/app/PalmPay/PalmPay.apk
rm -f product/operator/app/PalmPay/PalmPay.apk.* 2>/dev/null
cat product/operator/app/TikTokmusically/TikTokmusically.apk.* 2>/dev/null >> product/operator/app/TikTokmusically/TikTokmusically.apk
rm -f product/operator/app/TikTokmusically/TikTokmusically.apk.* 2>/dev/null
cat product/operator/app/TikToktrill/TikToktrill.apk.* 2>/dev/null >> product/operator/app/TikToktrill/TikToktrill.apk
rm -f product/operator/app/TikToktrill/TikToktrill.apk.* 2>/dev/null
cat product/operator/app/Wps/Wps.apk.* 2>/dev/null >> product/operator/app/Wps/Wps.apk
rm -f product/operator/app/Wps/Wps.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/priv-app/Messages/Messages.apk.* 2>/dev/null >> product/priv-app/Messages/Messages.apk
rm -f product/priv-app/Messages/Messages.apk.* 2>/dev/null
cat product/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null >> product/priv-app/Phonesky/Phonesky.apk
rm -f product/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null
cat product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> product/priv-app/Velvet/Velvet.apk
rm -f product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
